#include "cpu.h"
#include "memory.h"
#include <stdio.h>

//functions
//add ALU
int addALU(int a, int b)
{
    return a + b;
};

int subtractALU(int a, int b)
{
    return a - b;
};

// or ALU
int orALU(int a, int b)
{
    return a | b;
};

//and ALU
int andALU(int a, int b)
{
    return a & b;
};

int nextPc_adder(int pc_address)
{
    return *pc_address + 0x4;
};

int controlcompareSignal(a, b)
{
    if ((a - b) > 0)
    {
        return 1;
    }
}
