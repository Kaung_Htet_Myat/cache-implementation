#include "cpu.h"
#include "clocks.h"
#include "memory.h"
#include "syscall.h"
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#define DEBUG

int main(int argc, char *argv[])
{
	typedef unsigned long ticks;
    #number of stalls
    long num_stalls = 0;
    long cycles = ticks getticks(void);
    FILE *f;
	struct IF_ID_buffer if_id;
	struct ID_EX_buffer id_ex;
	struct EX_MEM_buffer ex_mem;
	struct MEM_WB_buffer mem_wb;
	int i;
  size_t count;

  /* Set the PC */
	cpu_ctx.PC = 0x00400000;

/* Initialize register to 0 */
	for ( i = 0; i < 32; i++ ) {
		cpu_ctx.GPR[i] = 0;
	}
 
 /* Set the SP */
  cpu_ctx.GPR[29] = 0x10002000;

	/* Read memory from the input file */
	f = fopen(argv[1], "r");
	assert(f);
  count = fread(data_memory, sizeof(uint32_t), 1024, f);
  assert(count == 1024);
  count = fread(instruction_memory, sizeof(uint32_t), 1024, f);
  assert(count == 1024);
	fclose(f);

	while(1) {
#if defined(DEBUG)
		printf("FETCH from PC=%x\n", cpu_ctx.PC);
#endif
		writeback( &mem_wb );
		memory( &ex_mem, &mem_wb );
		execute( &id_ex, &ex_mem );
		decode( &if_id, &id_ex );
		fetch( &if_id );
	
		if ( cpu_ctx.PC == 0 ) break;
	}

    return 0;
}

//stall counter adder
void stall_counter_adder(){
    num_stalls = num_stalls + 1;
}

//cycle counter adder
void cycle_counter_adder(){
    cycles = cycles + 1;
}
