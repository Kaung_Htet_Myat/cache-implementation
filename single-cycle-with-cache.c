#include "cpu.h"
#include "memory.h"
#include "syscall.h"
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

void CalculateCPI(int cycles, int iCacheMisses, int dCacheMisses, int instructions)
{
    int baseCPI = 1;
    float istalls = (1 * iCacheMisses / instructions) * cycles;
    float dstalls = (1 * dCacheMisses / instructions) * cycles;
    float CPI = baseCPI + istalls + dstalls;
    printf("CPI = %f", CPI);
}

void printCacheStatus(int stalls, int instructions, int cycles, int iCacheMisses, int dCacheMisses)
{
    printf("The number of stalls: %d\n", stalls);
    printf("The number of instructions: %d\n", instructions);
    printf("The number of cycles: %d\n", cycles);
    CalculateCPI(cycles, iCacheMisses, dCacheMisses, instructions);
}

//if the index is empty a hit, else if the values are same, its a hit, if not it is a miss.
void directmapwrite(L1_CAHCES *L1, unsigned int *address, int *miss, int *hit, int *stalls)
{
    unsigned int data = *address;
    if (*L1.dCache.entries[address % (256 / 4)] != NULL)
    {
        *stalls = *stalls + 1;
        *miss = *miss + 1;
        L1.dCache.entris[address % (256 / 4)] = data;
    }
    else
    {
        if (data == L1.dCache.entris[address % (256 / 4)])
        {
            *hit = *hit + 1;
        }
        else
        {
            *stalls = *stalls + 1;
            *miss = *miss + 1;
            L1.dCache.entris[address % (256 / 4)] = data;
        }
    }
}

//if data is empty its a miss, if not it is a hit, if the data is equal, set lru every hit and miss.
unsigned int directmapread(L1_CAHCES *L1, unsigned int *address, int *miss, int *hit, int *stalls, char *lru)
{
    data = *L1.dCache.entries[address % (256 / 4)];
    if (*address == data)
    {
        *lru[address % (256 / 4)] = 1; //set lru to one, its a hit
        *hit = *hit + 1;
    }
    else
    {
        *stalls = *stalls + 1;
        *miss = *miss + 1;
        *L1.dCache.entries[address % (256 / 4)] = *address;
        *lru[address % (256 / 4)] = 1;
    }

    return *lru[address % (256 / 4)];
}

int directmapfetch(struct IF_ID_buffer *out)
{
    If_ID_buffer address = *out.next_pc;
    if (*L1.dCache.entries[address % (256 / 4)] == *out.next_pc)
    {
        *lru[address % (256 / 4)] = 1; //set lru to one, its a hit
        *hit = *hit + 1;
    }
    else
    {
        *stalls = *stalls + 1;
        *miss = *miss + 1;
        *L1.dCache.entries[address % (256 / 4)] = *address;
        *lru[address % (256 / 4)] = 1;
    }

    return address;
}

void fetchC(struct IF_ID_buffer *out)
{
    *out.next_pc = nextPc_adder(*out.next_pc);
    int x = directmapfetch(*out.next_pc);

    return 0;
}

int memoryC(struct EX_MEM_buffer *in, struct MEM_WB_buffer *out)
{
    if (*in.readRegister != 0)
    {
        *in.addresstoAccess = get_imm(*in_instruction);
        *in.rs = directmapread(*L1, *address, *miss, *hit, *stalls, *lru);
    }
    elif (*in.writeRegister != 0)
    {
        *int.addresstoAccess = get_imm(*in_instruction);
        *out.rs = directmapread(*L1, *address, *miss, *hit, *stalls, *lru);
    }
    return 0;
}

int main()
{
    L1_CACHES L1;
    int stalls, hits, miss;
    char lru[256];

    FILE *f;
    struct IF_ID_buffer if_id;
    struct ID_EX_buffer id_ex;
    struct EX_MEM_buffer ex_mem;
    struct MEM_WB_buffer mem_wb;
    int i;
    size_t count;

    /* Set the PC */
    cpu_ctx.PC = 0x00400000;

    /* Initialize register to 0 */
    for (i = 0; i < 32; i++)
    {
        cpu_ctx.GPR[i] = 0;
    }

    /* Set the SP */
    cpu_ctx.GPR[29] = 0x10002000;

    /* Read memory from the input file */
    f = fopen(argv[1], "r");
    assert(f);
    count = fread(data_memory, sizeof(uint32_t), 1024, f);
    assert(count == 1024);
    count = fread(instruction_memory, sizeof(uint32_t), 1024, f);
    assert(count == 1024);
    fclose(f);

    while (1)
    {
#if defined(DEBUG)
        printf("FETCH from PC=%x\n", cpu_ctx.PC);
#endif

#if defined(efined(ENABLE_L1_CACHES)
        fetchC(&if_id);
        decode(&if_id, &id_ex);
        execute(&id_ex, &ex_mem);
        memoryC(&ex_mem, &mem_wb);
        writeback(&mem_wb);
        if (cpu_ctx.PC == 0)
            break;

        printCacheStatus(stalls, instructions, cycles, iCacheMisses, dCacheMisses)
#endif
            fetch(&if_id);
        decode(&if_id, &id_ex);
        execute(&id_ex, &ex_mem);
        memory(&ex_mem, &mem_wb);
        writeback(&mem_wb);
        if (cpu_ctx.PC == 0)
            break;
    }

    return 0;
}